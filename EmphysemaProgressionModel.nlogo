extensions [csv]

globals [ index hours t smoke ix dt dtX dtY hidden-links? initial-elastic-force]


breed[nodes node]
breed[particles particle]
breed[macrophages macrophage]
breed[fibroblasts fibroblast]
breed[Tcells Tcell]

turtles-own [life]

patches-own
[
  tissue-life
  collagen
  pro-cytokine
  anti-cytokine
  act-cytokine
  emphysema?
  healthy?
  ]

links-own
[
 k
 elastic-force
 elastic-forceX
 elastic-forceY
 delta
 deltaX
 deltaY
 dist
 distX
 distY
 initial-heading
 coordX
 coordY
 lnkheading
 ]

nodes-own
[
  external-force
  sum-of-forcesX
  sum-of-forcesY
  mass
  acceleration
  displacement
  force-modulus
  force-angle
  ]

macrophages-own[activation]
Tcells-own[activation]
particles-own[clearance]

;;;;;;;;;;;;M;;;;;;A;;;;;;;;;I;;;;;;;N;;;;;;;;;;;;;;;;;;;;;;;

to setup
  clear-all
  reset-ticks

  set dt 0.1
  set initial-elastic-force (1 * (2 - length-at-rest))

  set-default-shape particles "star"
  set-default-shape nodes "dot"
  set-default-shape Macrophages "circle"
  set-default-shape Fibroblasts "pentagon"
  set index 0

  set t 0
  set ix 0



ask patches [
               set pcolor green
               set tissue-life 100
               set emphysema? false
               set healthy? true
               set collagen 0
               set pro-cytokine 0
               set anti-cytokine 0
               coloring
              ]

  create-macrophages round(0.3 * (count patches) / 10)  ;There is 1 alveolar macrophage each
    [
     set color white
     set life macrophage-life + random 200
     set activation 0
     setxy random-xcor random-ycor
    ]

  create-fibroblasts round(0.1 * (count patches) / 10)
    [
     set color blue
     set life fibroblast-life + random 200
     setxy random-xcor random-ycor
    ]

  setup-square
end



to go

   ask particles [
     particles-function
     ]

   ask macrophages [
     macrophages-function
     ]

   ask fibroblasts [
     fibroblasts-function
     ]

   ask Tcells[
     Tcells-function
   ]

   evaporate
   diffusion

   if (t < 1500)[
     if (t mod period = 0)[
       randomxy-particles
     ]
   ]

    ask turtles
   [
    set life life - 1
    if life = 0
      [die]
   ]

  let basalNumMacrophages round(0.3 * (count patches) / 10)

  if (count Macrophages < basalNumMacrophages)
  [
   create-macrophages (basalNumMacrophages - (count Macrophages))
    [
     set color white
     set life macrophage-life + random 200
     set activation 0
     setxy random-xcor random-ycor
    ]
  ]

   let basalNumFibroblasts round(0.1 * (count patches) / 10)

   if count fibroblasts < basalNumFibroblasts
   [
    create-fibroblasts (basalNumFibroblasts - (count Fibroblasts))
     [
      set color blue
      set life Fibroblast-life + random 200
      setxy random-xcor random-ycor
     ]
   ]

   callfibros
   callmacros
   if (t > 500) [callTcells]

   calculate-index

   ask patches [coloring]

   go-mechanical-action

   if (t mod 500 = 0)[
     mechanic-breakdown2
     break-tissue]

   if (plots? = true) [do-plots]

   tick
   set t t + 1
end


to go-mechanical-action
  ask nodes with [color = red] [
    compute-elastic-force
    compute-sum-of-forces
    compute-displacement
    fd abs(displacement)
    ]

end




;;;;;;;;;;;;;;;;;;;;;;;;;;P;;;R;;;O;;;C;;;E;;;D;;;U;;;R;;;E;;;S;;;;;;;;;;;;;;;;;;;;;;;;;;


to calculate-index
  ask patches [if (tissue-life < 10) [set emphysema? true set healthy? false]]
  let healthy-pixels count patches with [healthy? = true]
  let emphysema-pixels count patches with [emphysema? = true]
  set index emphysema-pixels / (emphysema-pixels + healthy-pixels)
end



to macrophages-function

  ;; Macrophags move towards higher levels of pro-inflammatory cytokines by chemotaxis
  if (not any? particles-here)[

  ifelse ([pro-cytokine] of max-one-of neighbors [pro-cytokine] - [pro-cytokine] of patch-here > 5) [

    ifelse ([pro-cytokine] of max-one-of neighbors [pro-cytokine] > [pro-cytokine] of patch-here)[
             set heading towards max-one-of neighbors [pro-cytokine]
      fd 1
    ]

    [ random-walk ]
  ]

    [ random-walk ]
  ]


    if activation = 0 and random 100 < 10
       [ask patch-at 0 0 [set pro-cytokine pro-cytokine + random 50]]
    if activation = 0 and random 100 < 10
       [ask patch-at 0 0 [set anti-cytokine anti-cytokine + random 30]]

    if activation > 5
      [ask patch-at 0 0 [set pro-cytokine pro-cytokine + (50 - random ([anti-cytokine] of patch-at 0 0))]]

    if activation < 5 and activation > 0
      [ask patch-at 0 0 [set anti-cytokine anti-cytokine + 30]]

;; Activation because they found a particle
    ifelse (count particles-on patch-at 0 0) > 0 or (count particles-on patch-ahead 1) > 0 or
    (count particles-on patch-right-and-ahead 45 1) > 0 or (count particles-on patch-left-and-ahead 45 1) > 0
      [
        set activation 50

      ]
      [
       if activation > 0
       [set activation activation - 1]
      ]

;; Activation due to activating cytokines

     ifelse (([act-cytokine] of patch-at 0 0) > 15)
      [
        set activation 50

      ]
      [
       if activation > 0
       [set activation activation - 1]
      ]

;;Activation due to large presence of collagen
      ifelse ([collagen] of patch-at 0 0) > 20
      [set activation 20

      ]
      [
       if activation > 0
       [set activation activation - 1]
      ]

end

to fibroblasts-function

  ;; Fibroblasts move towards lower levels of tissue life
  ifelse ([tissue-life] of min-one-of neighbors [tissue-life] - [tissue-life] of patch-here > 0.5) [

  ifelse ([tissue-life] of min-one-of neighbors [tissue-life] < [tissue-life] of patch-here)[
    set heading towards min-one-of neighbors [tissue-life]
    fd 1
    ]
    [ random-walk ]
  ]

    [ random-walk ]


  if ([tissue-life] of patch-at 0 0) < 98 and ([collagen] of patch-at 0 0 ) < 100
    [
      ask patch-at 0 0
        [set tissue-life tissue-life + 1
         set collagen collagen + 0.25]
    ]

end


to particles-function

  set pro-cytokine pro-cytokine + random 30

    if clearance = 1
      [set life life - ((count macrophages-on patch-at 0 0))
       if life < 0
         [die]
      ]

    if (count macrophages-on patch-at 0 0) > 0 and clearance = 0
      [set life 10
       set clearance 1]

end


to Tcells-function

  ; Tcells movement towards dameged tissue

  if activation = 0 [random-walk]

  if (activation > 0) [

    if mean [tissue-life] of patches < 75 [

      ifelse ([tissue-life] of min-one-of neighbors [tissue-life] < [tissue-life] of patch-here)[

        set heading towards max-one-of neighbors [pro-cytokine]
        fd 1

  ]
      [random-walk]
    ]
  ]


  ifelse (count macrophages-on patch-at 0 0) > 0 or (count macrophages-on patch-ahead 1) > 0 or (count macrophages-on patch-right-and-ahead 45 1) > 0 or (count macrophages-on patch-left-and-ahead 45 1) > 0
      [

        set activation life

      ]
      [
       if activation > 0
       [set activation activation - 1]
      ]

  if (activation > 0)
    [
      ask patch-at 0 0
        [set act-cytokine act-cytokine + random 80
         set pro-cytokine pro-cytokine + random 50]
    ]

if (random-float 1 < 0.2)[
  if (activation < 2 and activation > 0 and [tissue-life] of patch-at 0 0 < 50)
  [ hatch-Tcells 1
    [
      set life Tcell-life + random 200
      set activation life]
    ]
]

end

to random-walk
  rt random 360
  fd 1
end


to diffusion
  diffuse pro-cytokine diffusion-constant
  diffuse anti-cytokine diffusion-constant
  diffuse act-cytokine diffusion-constant
end


to randomxy-particles
  create-particles num-particles
  [
    set color brown
    setxy random-xcor random-ycor
  ]
end


to evaporate

  ask patches
    [
     if tissue-life > 0
       [set tissue-life tissue-life - (pro-cytokine / 75)]

     set pro-cytokine pro-cytokine * 0.7
     set act-cytokine act-cytokine * 0.7
     set anti-cytokine anti-cytokine * 0.9
     set collagen collagen * 0.85
     if tissue-life < 100 [set tissue-life tissue-life * 1.0003]

     if tissue-life < 0
       [set tissue-life 0]

     if tissue-life > 100
       [set tissue-life 100]
    ]

end


to callmacros
   if count macrophages < 400
   [
     if (mean [collagen] of patches) > 5 or (mean [pro-cytokine] of patches) > 2
     [
     create-macrophages (round (((mean [collagen] of patches) + (mean [pro-cytokine] of Patches) - (mean [anti-cytokine] of Patches)) / 3 / 2))

      [
       set color white
       set life macrophage-life + random 200
       set activation 0
       setxy random-xcor random-ycor
      ]
     ]

     if (count particles) > 10
     [create-macrophages random 2
      [
       set color white
       set life macrophage-life + random 200
       set activation 0
       setxy random-xcor random-ycor
      ]
     ]
   ]
end


to callfibros
   if count fibroblasts < 150 [
   if (mean [anti-cytokine] of patches) > 1
   [create-fibroblasts (round (mean [anti-cytokine] of patches))
    [
     set color blue
     set life fibroblast-life + random 200
     setxy random-xcor random-ycor
    ]
   ]
    ]
end

to callTcells
  if count Tcells < 50 [
     if (count macrophages with [activation > 0] > (0.5 * count macrophages) and (random 100 < 10))[
         create-Tcells (round (100 - mean [tissue-life] of patches))
         [
           set color red
           set life Tcell-life + random 200
           setxy random-xcor random-ycor
         ]
     ]
     ]
end


;to diffusion [substance constant max-substance]
;    set dtX sum [substance] of (patch-set patch-at -1 0 patch-at 1 0)
;    set dtX dtX - 2 * [substance] of patch-at 0 0
;    set dtY sum [substance] of (patch-set patch-at 0 -1 patch-at 0 1)
;    set dtY dtY - 2 * [substance] of patch-at 0 0
;    set substance substance + constant * dt * ( dtX + dtY )
;    if substance > max-substance[ set substance max-substance]
;end


to coloring
  set pcolor scale-color green  tissue-life 0 250
end



;;;;;;;;;;;;;;;;Procedures for the mechanic part only;;;;;;;;;;;;;;;;;;;;

to setup-square

  ask patches with [(pxcor mod 2) = 0 and (pycor mod 2) = 0][ sprout-nodes 1 ]

  ask nodes with [pxcor = max-pxcor or pxcor = min-pxcor or pycor = max-pycor or pycor = min-pycor and not((pxcor = max-pxcor and pycor = max-pycor) or (pxcor = min-pxcor and pycor = max-pycor) or (pxcor = max-pxcor and pycor = min-pycor) or (pxcor = min-pxcor and pycor = min-pycor))][
      let nodeSubset min-n-of 3 other nodes [distance myself]
      create-links-to nodeSubset
  ]

  ask nodes with [pxcor != max-pxcor and pxcor != min-pxcor and pycor != max-pycor and pycor != min-pycor][
      let nodeSubset min-n-of 4 other nodes [distance myself]
      create-links-to nodeSubset
  ]

  ask nodes with [(pxcor = max-pxcor and pycor = max-pycor) or (pxcor = min-pxcor and pycor = max-pycor) or (pxcor = max-pxcor and pycor = min-pycor) or (pxcor = min-pxcor and pycor = min-pycor)]
  [
      let nodeSubset min-n-of 2 other nodes [distance myself]
      create-links-to nodeSubset
  ]

  ask links[
    set color grey
    set length-at-rest 1
    set initial-heading link-heading
    set k 1]

  ask nodes [
      set color red
      set size 0.25
      set mass 1
      if (xcor = max-pxcor or xcor = min-pxcor or ycor = max-pycor or ycor = min-pycor) [set color blue]
      set-links-coordX
      set-links-coordY
      ]
end



to compute-elastic-force

 ask my-out-links [

    set deltaX [xcor] of other-end - [xcor] of myself
    set distX abs(deltaX)
    set deltaY [ycor] of other-end - [ycor] of myself
    set distY abs(deltaY)

    set dist sqrt ((distX * distX) + (distY * distY))
    set elastic-force (1 * (dist - length-at-rest))

    if (deltaX = 0) [set elastic-forceX 0]
    if (deltaY = 0) [set elastic-forceY 0]

    ifelse (deltaX > 0 and deltaY > 0)
    [set elastic-forceX elastic-force * cos (90 - link-heading  ) ]
    [set elastic-forceY  elastic-force * sin (90 - link-heading) ]

    ifelse (deltaX < 0 and deltaY > 0 )
    [set elastic-forceX elastic-force * (- cos (link-heading - 270 ))  ]
    [set elastic-forceY elastic-force *   sin (link-heading - 270 )  ]

    ifelse (deltaX > 0 and deltaY < 0 )
    [set elastic-forceX elastic-force *  cos (link-heading - 90 ) ]
    [set elastic-forceY elastic-force * (-  sin (link-heading - 90 ) ) ]

    ifelse (deltaX < 0 and deltaY < 0 )
    [set elastic-forceX elastic-force * (- cos (90 - (link-heading - 180)))]
    [set elastic-forceY elastic-force * (-  sin (90 - (link-heading - 180)))]
 ]

end


to compute-sum-of-forces
  set sum-of-forcesX (sum [elastic-forceX] of my-out-links)
  set sum-of-forcesY (sum [elastic-forceY] of my-out-links)
  set force-modulus sqrt ( (sum-of-forcesX * sum-of-forcesX) + (sum-of-forcesY * sum-of-forcesY) )
  if (sum-of-forcesX != 0 and sum-of-forcesY != 0) [
    set force-angle atan sum-of-forcesX sum-of-forcesY
    set heading force-angle
  ]
end

to compute-displacement
  set acceleration force-modulus / mass
  set displacement 0.5 * (acceleration * (dt * dt))
end


to mechanic-breakdown2
  ask links [if (elastic-force > 3 * initial-elastic-force) [die]]
  ask links [if ([emphysema?] of patch coordX coordY) [die]]
  ask nodes [if ([emphysema?] of patch-here) [ die ]]
end


to set-links-coordY
  ask my-out-links with [link-heading = 0 or link-heading = 180][
    set delta [ycor] of other-end - [ycor] of myself
    set dist abs(delta)
    ifelse (delta > 0)                      ; Is the neighbor to your right or left?
    [ set coordY [ycor] of myself + 1 set coordX [xcor] of myself]  ;;
    [ set coordY [ycor] of myself - 1 set coordX [xcor] of myself]
    ]
end

to set-links-coordX
  ask my-out-links with [link-heading = 90 or link-heading = 270][
    set delta [xcor] of other-end - [xcor] of myself
    set dist abs(delta)
    ifelse (delta > 0)                      ; Is the neighbor to your right or left?
    [ set coordX [xcor] of myself + 1 set coordY [ycor] of myself]  ;;
    [ set coordX [xcor] of myself - 1 set coordY [ycor] of myself]
    ]
end

to break-tissue
  ask patches [


      let pcoordX pxcor
      let pcoordY pycor

       if (not(any? (nodes in-radius 1.45) ))  ;with [any? links with [round(coordX) = pcoordX and round(coordY) = pcoordY]]))
        [
             set tissue-life 0
             set pcolor red
             ]

    ]
end

;; Other procedures;;;

to hide-links
    ask links[hide-link]
end

to show-links
   ask links[show-link]
end

to do-plots
   set-current-plot "Tissue"
   set-current-plot-pen "index"
   plot index
   set-current-plot-pen "tislife"
   plot .01 * mean [tissue-life] of patches

   set-current-plot "Molecular variations"
   set-current-plot-pen "pro"
   plot mean [pro-cytokine] of patches
   set-current-plot-pen "anti"
   plot mean [anti-cytokine] of patches
   set-current-plot-pen "activ"
   plot mean [act-cytokine] of patches
   set-current-plot-pen "collagen"
   plot mean [collagen] of patches

   set-current-plot "Cells"
   set-current-plot-pen "macro"
   plot count macrophages
   set-current-plot-pen "fibro"
   plot count fibroblasts
   set-current-plot-pen "T cells"
   plot count Tcells

end
@#$#@#$#@
GRAPHICS-WINDOW
290
38
730
479
-1
-1
8.471
1
10
1
1
1
0
1
1
1
0
50
0
50
0
0
1
ticks
30.0

BUTTON
23
43
86
76
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
123
42
186
75
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
289
512
371
545
NIL
hide-links
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
387
511
475
544
NIL
show-links
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
27
114
100
174
period
75.0
1
0
Number

INPUTBOX
120
113
195
173
num-particles
25.0
1
0
Number

TEXTBOX
29
90
179
109
Exposure \n
15
0.0
1

SLIDER
26
225
198
258
macrophage-life
macrophage-life
200
1000
1000.0
100
1
NIL
HORIZONTAL

SLIDER
26
269
198
302
fibroblast-life
fibroblast-life
200
1000
200.0
100
1
NIL
HORIZONTAL

SLIDER
26
315
198
348
Tcell-life
Tcell-life
200
1000
1000.0
100
1
NIL
HORIZONTAL

SLIDER
30
408
202
441
diffusion-constant
diffusion-constant
0.25
0.75
0.25
0.05
1
NIL
HORIZONTAL

SLIDER
30
482
202
515
length-at-rest
length-at-rest
0.1
1
1.0
0.1
1
NIL
HORIZONTAL

TEXTBOX
29
195
179
213
Agents lifespan
14
0.0
1

TEXTBOX
31
385
181
403
Cytokines diffusion
14
0.0
1

TEXTBOX
37
461
187
479
Level of spring pre-stress\n
12
0.0
1

SWITCH
496
513
599
546
plots?
plots?
1
1
-1000

PLOT
773
38
1263
201
Tissue
iterations
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"index" 1.0 0 -16777216 true "" ""
"tislife" 1.0 0 -7500403 true "" ""

PLOT
773
210
1266
373
Molecular variations
iterations
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"pro" 1.0 0 -16777216 true "" ""
"anti" 1.0 0 -7500403 true "" ""
"activ" 1.0 0 -2674135 true "" ""
"collagen" 1.0 0 -955883 true "" ""

PLOT
773
379
1266
544
Cells
iterations
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"macro" 1.0 0 -16777216 true "" ""
"fibro" 1.0 0 -7500403 true "" ""
"T cells" 1.0 0 -2674135 true "" ""

@#$#@#$#@
## WHAT IS IT?

The aim of this model is to reproduce the main processes taking place in the lung parenchyma due to the exposure to irritants such as tobacco smoke. Therefore, allowing us to observe the evolving progression of emphysema.

## HOW IT WORKS

Although it is still not clear which are the exact pathophysiological processes underlying the non-reversible progression of the disease, there are two main lines of research trying to find an answer to this question. The first one is focused on the study of the immune response to the particulate exposure [1] and the other is centered on the relevance of mechanical forces as a possible responsible of the tissue breakdown [2].

This model integrates both hypotheses in order to study the relevance they have in the progression of the disease. Also, this model can be served to perform a sensitivity analysis of the outcomes of modifying different parameters in order to investigate their relevance in the model.

1) Infammatory response to irritants[12]


	- A number of particles are deposited on the tissue in a random manner, with a frequence of depositation variable and related to the tobacco smoking frequency.
	- Every particle creates a gradient of inflammatory citokines (TNF-α, IL-6)
	- Simulating the effect of chemotaxis, macrophages will migrate to the particles location in an up-gradient manner[3].
	- Once particle and macrophage encounter, macrophages remain in the same patch until the removal of the particle and become activated during a time-lapse of 50 iterations.[4,5],
	- While the macrophage is activated, it will move around the environment releasing higher levels of pro-inflammatory cytokines during all the iterations but the last five ones, when the higher release of pro-cytokines is substituted for a higher level of anti-inflammatory cytokines (TGF-β1). [6,7]
	- The amount of inflammatory cytokines is directly related to the level of tissue damage.
	- In turn, fibroblast will move towards the damaged areas, repairing it (i.e increasing tissue life) and depositing collagen, which also can activate macrophages, thus increasing the tissue damage. [8]


2) Mechanical forces[11]

	- This model consists on a grid of 26 x 26 nodes each of them unified with its four main neighbors using links in Netlogo altogether constructing a simplified web of elastic springs (spring-particle model) that in a lumped model manner are willing to behave as the parenchymal elastic tissue of the lung, in concrete reproducing the properties of collagen fibrils, which has been shown to be described by Hooke’s law for elastic springs. [9, 10]

	- The external nodes of the grid are fixed while all the rest are free to move following Newton’s laws of motion. Considering that each of the four links/elastic springs are following Hooke’s law for elastic springs and that mass is equal to one in all of them, they have been pre-stressed by assigning a length-at-rest smaller than their length at the initial equilibrium.

	- At the initial position, the sum of forces at each node equals zero in both the X and Y axis. When a link or a node is gone due to the weakness of the underlying tissue, it is created disequilibrium in the surrounding nodes. Therefore, the sum of forces at those nodes will be different to zero and there will be an acceleration given by the Newton’s second law of motion (F = m • a) and the node will move until a new equilibrium is found.
When a spring is gone, the sum of forces at its nodes will be different to zero, so the turtles will start to move in the direction of the resulting forces.


## HOW TO USE IT

The first thing to do is to adjust all the parameters showed in the user interface.

-Exposure (period and number of particles) it determines the frequency and quantity of smoking the individual represented in the model is having.
- Agents lifespan (macrophage-life, fibroblast-life, TCell-life): They represent the number of iterations each of the agent lasts once ther are created.
- Cytokines diffusion: it is the rate of diffusion of all the inflammatory cytokines.
- Level of spring prestress: Defined with the length-at-rest of the links, it will determine the level of implication of the elastic forces in the tissue breakdown. The smaller is the length-at-rest, the larger the prestress will be so the more easy it will be that the links break.

Then you click on 'set up', and finally 'go'.



## CREDITS AND REFERENCES

[1]  Cosio, Manuel G., Marina Saetta, and Alvar Agusti. "Immunologic aspects of chronic obstructive pulmonary disease." New England Journal of Medicine360.23 (2009): 2445-2454.
[2] Béla Suki, Kenneth R. Lutchen, and Edward P. Ingenito "On the Progressive Nature of Emphysema",American Journal of Respiratory and Critical Care Medicine, Vol. 168, No. 5 (2003), pp. 516-521.
[3] Massion PP, Inoue H, Richman-Eisenstat J, et al. Novel Pseudomonas product stimulates interleukin-8 production in airway epithelial cells in vitro. J Clin Invest 1994; 93: 26–32
[4] Hogan, Simon P., et al. "Eosinophils: biological properties and role in health and disease." Clinical & Experimental Allergy 38.5 (2008): 709-750.
[5] Segal, Anthony W. "How neutrophils kill microbes." Annual review of immunology 23 (2005): 197.
[6]  A. Mantovani, A. Sica, S. Sozzani, P. Allavena, A. Vecchi, et al., The chemokine system in diverse forms of macrophage activation and polarization, Trends Immunol. 25 (2004) 677
[7] F. Porcheray, S. Viaud, A.C. Rimaniol, C. Leone, B. Samah, et al., Macrophage activation switching: an asset for the resolution of inflammation, Clin. Exp. Immunol. 142 (2005) 481.
[8] Prasse, Antje, et al. "A vicious circle of alveolar macrophages and fibroblasts perpetuates pulmonary fibrosis via CCL18." American journal of respiratory and critical care medicine 173.7 (2006): 781-792.
[9] Sasaki, Naoki, and Singo Odajima. "Stress-strain curve and Young's modulus of a collagen molecule as determined by the X-ray diffraction technique."Journal of biomechanics 29.5 (1996): 655-658.
[10] Shen, Zhilei L., et al. "Stress-strain experiments on individual collagen fibrils."Biophysical Journal 95.8 (2008): 3956-3963.
[11] Béla Suki, Kenneth R. Lutchen, and Edward P. Ingenito "On the Progressive Nature of Emphysema",American Journal of Respiratory and Critical Care Medicine, Vol. 168, No. 5 (2003), pp. 516-521.
[12]Brown, Bryan N., et al. "An agent-based model of inflammation and fibrosis following particulate exposure in the lung." Mathematical biosciences 231.2 (2011): 186-196.

@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment1" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment2" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment3" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment4" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment5" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment6" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment7" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment8" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment9" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment10" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment11" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment12" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment13" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment14" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment15" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment16" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3001"/>
    <metric>index</metric>
    <metric>mean [pro-cytokine] of patches</metric>
    <metric>mean [anti-cytokine] of patches</metric>
    <metric>mean [act-cytokine] of patches</metric>
    <metric>mean [collagen] of patches</metric>
    <metric>mean [tissue-life] of patches</metric>
    <enumeratedValueSet variable="period">
      <value value="75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-particles">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="macrophage-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fibroblast-life">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Tcell-life">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="diffusion-constant">
      <value value="0.25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="length-at-rest">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
