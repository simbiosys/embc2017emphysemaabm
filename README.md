# README #

### What is this repository for? ###
This model reproduces the main processes taking place in the lung parenchyma due to the exposure to irritants such as tobacco smoke and allows us to observe the progression of emphysema.

* The model is described in detail in:
"Multi-scale Immunological and Biomechanical Model of Emphysema Progression" submitted to EMBC 2017
The paper is currently under review. If accepted, we will post the link as well

### How it works ###
Although it is still not clear which are the exact pathophysiological processes underlying the non-reversible progression of the disease, there are two main lines of research trying to find an answer to this question. The first one is focused on the study of the immune response to the particulate exposure [1] and the other is centered on the relevance of mechanical forces as a possible responsible of the tissue breakdown [2].
This model integrates both hypotheses in order to study the relevance they have in the progression of the disease. Also, this model can be served to perform a sensitivity analysis of the outcomes of modifying different parameters in order to investigate their relevance in the model.
1) Infammatory response to irritants[12]

- A number of particles are deposited on the tissue in a random manner, with a frequence of depositation variable and related to the tobacco smoking frequency.

- Every particle creates a gradient of inflammatory citokines (TNF-α, IL-6)
- Simulating the effect of chemotaxis, macrophages will migrate to the particles location in an up-gradient manner[3].
- Once particle and macrophage encounter, macrophages remain in the same patch until the removal of the particle and become activated during a time-lapse of 50 iterations.[4,5],
- While the macrophage is activated, it will move around the environment releasing higher levels of pro-inflammatory cytokines during all the iterations but the last five ones, when the higher release of pro-cytokines is substituted for a higher level of anti-inflammatory cytokines (TGF-β1). [6,7]
- The amount of inflammatory cytokines is directly related to the level of tissue damage.
- In turn, fibroblast will move towards the damaged areas, repairing it (i.e increasing tissue life) and depositing collagen, which also can activate macrophages, thus increasing the tissue damage. [8]
2) Mechanical forces[11]
- This model consists on a grid of 26 x 26 nodes each of them unified with its four main neighbors using links in Netlogo altogether constructing a simplified web of elastic springs (spring-particle model) that in a lumped model manner are willing to behave as the parenchymal elastic tissue of the lung, in concrete reproducing the properties of collagen fibrils, which has been shown to be described by Hooke’s law for elastic springs. [9, 10]

- The external nodes of the grid are fixed while all the rest are free to move following Newton’s laws of motion. Considering that each of the four links/elastic springs are following Hooke’s law for elastic springs and that mass is equal to one in all of them, they have been pre-stressed by assigning a length-at-rest smaller than their length at the initial equilibrium.

- At the initial position, the sum of forces at each node equals zero in both the X and Y axis. When a link or a node is gone due to the weakness of the underlying tissue, it is created disequilibrium in the surrounding nodes. Therefore, the sum of forces at those nodes will be different to zero and there will be an acceleration given by the Newton’s second law of motion (F = m • a) and the node will move until a new equilibrium is found.
When a spring is gone, the sum of forces at its nodes will be different to zero, so the turtles will start to move in the direction of the resulting forces.


### How do I get set up? ###

You will need [NetLogo](https://ccl.northwestern.edu/netlogo/download.shtml) to run this model
The first thing to do is to adjust all the parameters showed in the user interface.
-Exposure (period and number of particles) it determines the frequency and quantity of smoking the individual represented in the model is having. - Agents lifespan (macrophage-life, fibroblast-life, TCell-life): They represent the number of iterations each of the agent lasts once ther are created. - Cytokines diffusion: it is the rate of diffusion of all the inflammatory cytokines. - Level of spring prestress: Defined with the length-at-rest of the links, it will determine the level of implication of the elastic forces in the tissue breakdown. The smaller is the length-at-rest, the larger the prestress will be so the more easy it will be that the links break.
Then you click on ‘set up’, and finally ‘go’.

### Credits and references ###
[1] Cosio, Manuel G., Marina Saetta, and Alvar Agusti. “Immunologic aspects of chronic obstructive pulmonary disease.” New England Journal of Medicine360.23 (2009): 2445-2454. 

[2] Béla Suki, Kenneth R. Lutchen, and Edward P. Ingenito “On the Progressive Nature of Emphysema”,American Journal of Respiratory and Critical Care Medicine, Vol. 168, No. 5 (2003), pp. 516-521. 

[3] Massion PP, Inoue H, Richman-Eisenstat J, et al. Novel Pseudomonas product stimulates interleukin-8 production in airway epithelial cells in vitro. J Clin Invest 1994; 93: 26–32 

[4] Hogan, Simon P., et al. “Eosinophils: biological properties and role in health and disease.” Clinical & Experimental Allergy 38.5 (2008): 709-750. 

[5] Segal, Anthony W. “How neutrophils kill microbes.” Annual review of immunology 23 (2005): 197. 

[6] A. Mantovani, A. Sica, S. Sozzani, P. Allavena, A. Vecchi, et al., The chemokine system in diverse forms of macrophage activation and polarization, Trends Immunol. 25 (2004) 677 

[7] F. Porcheray, S. Viaud, A.C. Rimaniol, C. Leone, B. Samah, et al., Macrophage activation switching: an asset for the resolution of inflammation, Clin. Exp. Immunol. 142 (2005) 481. 

[8] Prasse, Antje, et al. “A vicious circle of alveolar macrophages and fibroblasts perpetuates pulmonary fibrosis via CCL18.” American journal of respiratory and critical care medicine 173.7 (2006): 781-792. 

[9] Sasaki, Naoki, and Singo Odajima. “Stress-strain curve and Young’s modulus of a collagen molecule as determined by the X-ray diffraction technique.”Journal of biomechanics 29.5 (1996): 655-658. 

[10] Shen, Zhilei L., et al. “Stress-strain experiments on individual collagen fibrils.”Biophysical Journal 95.8 (2008): 3956-3963. 

[11] Béla Suki, Kenneth R. Lutchen, and Edward P. Ingenito “On the Progressive Nature of Emphysema”,American Journal of Respiratory and Critical Care Medicine, Vol. 168, No. 5 (2003), pp. 516-521. [12]Brown, Bryan N., et al. “An agent-based model of inflammation and fibrosis following particulate exposure in the lung.” Mathematical biosciences 231.2 (2011): 186-196.

### Who do I talk to? ###
For any questions you can address Mario Ceresa and Silvia Fernandez